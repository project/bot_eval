
Usage
=====

Defaults to PHP.  Adds an opening php tag (<?php) if needed.

    <you> ?eval echo "Hello World!";
    <bot> http://codepad.org/3dmXczl9 Hello World!
    
You can specify other languages.
    
    <you> ?eval:python import time; print(time.time())
    <bot> http://codepad.org/go2hbYRZ 1303271492.62
